//
//  LoginModel.swift
//


import UIKit

struct SubmitModel: Decodable {
    let success: Bool?
    let message: String?
    let saved_message: saved_messageDetails
}
struct saved_messageDetails: Decodable {
    let __v: Int?
    let _id: String?
    let base64_message_image_string: String?
    let created_on: String?
    let full_name: String?
    let message: String?
    let message_code: String?
    let user_code: String?
    let user_id: String?
}

