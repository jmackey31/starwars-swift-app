//
//  Constants.swift
//

//
import UIKit


// API URL
struct Api
{
    // Base URL API
    static let kBaseUrl = "https://joshuadata.com/starwars_v1/"
}
struct Url
{
    // Base URL API
    static let new_Message = "message/new/"
}

struct UserData
{
    static let user_id = "5d7b1fd5f45d1900107acaec"
    static let user_code = "user_1568350165000754275"
    static let user_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBhbmthanJhd2F0IiwiaWF0IjoxNTY4NzIwNTA3LCJleHAiOjE1Njg4MDY5MDd9.985otSa1TkYfRTgZh6iUtd_lN_-wmKEoIzqbFsNhV2w"
}

