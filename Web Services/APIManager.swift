//
//  APIManager.swift
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class APIManager: NSObject {
    class func apiPost(url: String , parameters: [String:Any]?, completionHandler: @escaping (Data?, NSError?,JSON?) -> ()) {
        Alamofire.request("\(Api.kBaseUrl)\(url)" , method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.data{
                    print(response.result.value as Any)
                    completionHandler(data, nil, JSON(response.result.value ?? ""))
                }
                break
                
            case .failure(_):
                
                completionHandler(nil, response.result.error as NSError?,nil)
                break
                
            }
        }
        
    }
   
}



