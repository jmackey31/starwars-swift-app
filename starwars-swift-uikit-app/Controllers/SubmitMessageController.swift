//
//  SubmitMessageController.swift
//  starwars-swift-uikit-app
//
//  Copyright © 2019 Joshua Mackey. All rights reserved.
//

import UIKit
import SwiftyJSON

class SubmitMessageController: BaseController {
    
    var resData : SubmitModel?
    
    @IBOutlet var textField: CustomTextField!
    @IBOutlet var textView: CustomTextView!
    @IBOutlet var imageView: UIImageView!
    lazy var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
    //For corner radius of imageview
     imageView.layer.cornerRadius = 5.0
     imageView.clipsToBounds = true
     imageView.contentMode = .scaleAspectFill
     textView.textColor = UIColor.lightGray
     imagePicker.delegate = self
    }
    
    
    // MARK: - ImageViewTapped Action
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }

    @IBAction func submitButtonAction(_ sender: RoundedButton) {
        validations()
    }
    //MARK: - function to Check Validations
    func validations() {
         if imageView.image == UIImage(named: "selectImage") {
            self.simpleAlert("Alert", message: "Your Image can't be empty", actionBack: false)
        }else if self.range(text: textField.text ?? "").count == 0 {
            self.simpleAlert("Alert", message: "Your Name can't be empty", actionBack: false)
        }else if self.range(text: textView.text ?? "").count == 0 {
            self.simpleAlert("Alert", message: "Your Message can't be empty", actionBack: false)
        }else {
            // calling Submit Data Api function
            submitData()
        }
    }
    //MARK: - Submit Data Api function
    func submitData(){
        self.showProgress()
        let base64Image = convertImageToBase64(image: imageView.image!)
        print(base64Image)
        APIManager.apiPost(url: "\(Url.new_Message)\(UserData.user_id)/\(UserData.user_code)?token=\(UserData.user_token)", parameters: ["full_name": textField.text ?? "", "message": textView.text ?? "" ,"base64_message_image_string": base64Image ]) { (json:Data?, error:NSError?,SwiftyJson:JSON?) in
            self.dismissProgress()
            if error != nil {
                self.dismissProgress()
            }else{
                guard let data = json else { return }
                print(data)
                do {
                    self.resData = try JSONDecoder().decode(SubmitModel.self, from: data)
                    if self.resData?.success == true {
                        self.simpleAlert("Success", message: self.resData?.message ?? "", actionBack: false)
                    }else{
                        self.simpleAlert("Alert", message: self.resData?.message ?? "", actionBack: false)
                    }
                } catch let err {
                    self.simpleAlert("Alert", message: err.localizedDescription, actionBack: false)
                    print("Err", err)
                }
            }
        }
    }
    
}

    // MARK: - ImagePicker Delegate
extension SubmitMessageController:UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imageView.image = pickedImage
        }
       dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }

    
}

// MARK: - TextView Delegate
extension SubmitMessageController:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if let color = textView.textColor  {
            if color.isEqual(UIColor.lightGray) {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type your message here"
            textView.textColor = UIColor.lightGray
        }
    }
    
}

