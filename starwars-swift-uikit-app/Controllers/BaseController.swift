//
//  BaseController.swift
//  starwars-swift-uikit-app
//

//  Copyright © 2019 Joshua Mackey. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseController: UIViewController , NVActivityIndicatorViewable {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: -  function to Show NVActivityIndicatorView Progress
    func showProgress()  {
        let size = CGSize(width: 60, height: 60)
        
        DispatchQueue.main.async {
            self.startAnimating(size, message: nil, type: NVActivityIndicatorType.ballScaleMultiple, color: .white ,minimumDisplayTime: 0, fadeInAnimation: nil)
        }
    }
    //MARK: - function to Dismiss NVActivityIndicatorView Progress
    func dismissProgress()  {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    //MARK: - function to Show Simple Alert
    func simpleAlert(_ title: String, message: String, actionBack: Bool){
        let alert = UIAlertController(title: title,message: message,preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel, handler: { (action) -> Void
            in
            if actionBack == true{
                self.navigationController?.popViewController(animated: true)
            }
        })
        // Restyle the view of the Alert
        alert.view.tintColor = UIColor.black // change text color of the buttons
        alert.view.backgroundColor = UIColor.white  // change background color
        alert.view.layer.cornerRadius = 15   // change corner radius
        // Add action buttons and present the Alert
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - function to Trimming String Text
    func range(text: String) -> String{
        let trimmedString = text.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedString
    }
    
    //MARK: - function to Check Email vaildation
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // function to convert image into Base64
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.jpegData(compressionQuality: 0.3)!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
}
